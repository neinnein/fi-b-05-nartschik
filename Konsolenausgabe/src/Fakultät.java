
public class Fakult�t {

	public static void main(String[] args) {
		System.out.println(
				  "1!   = 1                 =   1\n"
				+ "2!   = 1 * 2             =   2\n"
				+ "3!   = 1 * 2 * 3         =   6\n"
				+ "4!   = 1 * 2 * 3 * 4     =  24\n"
				+ "5!   = 1 * 2 * 3 * 4 * 5 = 120");
		
		System.out.printf( "%-5s", "1!" );
		System.out.printf( "=" );
		System.out.printf( "%-19s", " 1" );
		System.out.printf( "=" );
		System.out.printf( "%4s\n", "1" );
		
		System.out.printf( "%-5s", "2!" );
		System.out.printf( "=" );
		System.out.printf( "%-19s", " 1 * 2" );
		System.out.printf( "=" );
		System.out.printf( "%4s\n", "2" );
		
		System.out.printf( "%-5s", "3!" );
		System.out.printf( "=" );
		System.out.printf( "%-19s", " 1 * 2 * 3" );
		System.out.printf( "=" );
		System.out.printf( "%4s\n", "6" );    
		
		System.out.printf( "%-5s", "4!" );
		System.out.printf( "=" );
		System.out.printf( "%-19s", " 1 * 2 * 3 * 4" );
		System.out.printf( "=" );
		System.out.printf( "%4s\n", "24" );    
		
		System.out.printf( "%-5s", "5!" );
		System.out.printf( "=" );
		System.out.printf( "%-19s", " 1 * 2 * 3 * 4 * 5" );
		System.out.printf( "=" );
		System.out.printf( "%4s\n", "120" );    
		

	}

}
