﻿import java.text.DecimalFormat;
import java.util.*;

class Fahrkartenautomat
{
    private static Scanner tastatur;

	public static void main(String[] args)
    {
       tastatur = new Scanner(System.in);
       
       DecimalFormat f = new DecimalFormat("#0.00");
       
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       float ergebnis = (float) 0.0;
       
       System.out.print("Anzahl Fahrkarten: ");
       int anzahlFahrkarten = tastatur.nextInt();
       System.out.print("Zu zahlen: ");
       float zuZahlenderBetrag = tastatur.nextFloat();
       if (anzahlFahrkarten > 1 && anzahlFahrkarten < 10) {
       ergebnis = Kosten(anzahlFahrkarten, zuZahlenderBetrag);
       }
       else {
    	   anzahlFahrkarten = (int) 1.0;
    	   ergebnis = Kosten(anzahlFahrkarten, zuZahlenderBetrag);
       }
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = Geldeinwurf(ergebnis);

       
       
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
      
	for (int x = 0; x < anzahlFahrkarten; x++) {
      Fahrscheinausgabe();
      
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
	
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       Rueckgeld(rückgabebetrag);
	}
       
   }
 
	
	public static float Kosten(float x, float y) {
		float ergebnis = x * y;
		
		return ergebnis;
	}
	public static float Geldeinwurf(float ergebnis) {
		
		float eingezahlterGesamtbetrag = (float) 0.0;
	       
		while(eingezahlterGesamtbetrag < ergebnis)
	       {
			   DecimalFormat f = new DecimalFormat("#0.00");
	    	   System.out.println("Noch zu zahlen: " + f.format((ergebnis - eingezahlterGesamtbetrag)));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   float eingeworfeneMünze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	           
	       }
		return eingezahlterGesamtbetrag;
	}
	public static void Fahrscheinausgabe() {
		
		 
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("");
	}
	
	public static void Rueckgeld(float rückgabebetrag) {
		
		if(rückgabebetrag > 0.0)
	       {
			   DecimalFormat f = new DecimalFormat("#0.00");
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
		}
	}
